
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author WIN10
 */
public class Product {
    private int id;
    private String name;
    private double price;
    private String image;

    public Product(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public String getImage() {
        return image;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", image=" + image + '}';
    }
    
    
    public static ArrayList<Product> genProductList(){
        ArrayList<Product> list = new ArrayList<>();
        list.add(new Product(1,"espresso1",40,"1.png"));
        list.add(new Product(2,"espresso2",40,"2.png"));
        list.add(new Product(3,"espresso3",40,"3.png"));
        list.add(new Product(4,"espresso4",40,"4.png"));
        list.add(new Product(5,"espresso5",40,"5.png"));
        list.add(new Product(6,"espresso6",40,"1.png"));
        list.add(new Product(7,"espresso7",40,"2.png"));
        list.add(new Product(8,"espresso8",40,"3.png"));
        list.add(new Product(9,"espresso9",40,"4.png"));
        list.add(new Product(10,"espresso10",40,"5.png"));
        return list;
    }
    
}
